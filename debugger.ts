import { EventsSDK, Menu } from "./wrapper/Imports"
const debuggerMenu = Menu.AddEntryDeep(["Main", "Debugger"])
debuggerMenu.IsHidden = true
EventsSDK.on("Draw", () => debuggerMenu.IsHidden = false)
